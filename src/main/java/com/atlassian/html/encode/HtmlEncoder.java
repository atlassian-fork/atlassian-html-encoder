package com.atlassian.html.encode;

import com.atlassian.annotations.PublicApi;

import java.io.IOException;
import java.io.Writer;

/**
 * A class that encodes data in a performant and low memory way.  If the string contains no html encoded characters then
 * it can be returned as is. If it does contain some HTML encode-able characters then it falls back to encoding each
 * character.
 * <p/>
 * Hence memory is only reallocated if the string needs HTML encoding and never if it does not.
 * <p/>
 * See https://extranet.atlassian.com/pages/viewpage.action?pageId=1840218980 for more details on the strategy
 */
@PublicApi
public class HtmlEncoder
{
    static final String AMPERSAND = "&amp;";
    static final String DOUBLE_QUOTE = "&quot;";
    static final String GREATER_THAN = "&gt;";
    static final String LESS_THAN = "&lt;";
    static final String SINGLE_QUOTE = "&#39;";  // was &apos; CONF-6494


    /**
     * Encodes a string using the Big5 Html encoding rules
     *
     * @param text the txt to encode
     * @return the HTML encoded text
     */
    public static String encode(String text)
    {
        if (text == null)
        {
            return "";
        }

        // Scan for the first character that needs to be escaped.  If we
        // don't find one, then there is no need to copy the data at all.
        final int len = text.length();
        for (int j = 0; j < len; ++j)
        {
            final char c = text.charAt(j);
            switch (c)
            {
                case '\'':
                case '"':
                case '&':
                case '<':
                case '>':
                    return encodeHeavy(text, j);
            }
        }
        return text;
    }

    private static String encodeHeavy(final String text, int j)
    {
        // Create the buffer with some extra space and catch up
        final int len = text.length();
        final StringBuilder str = new StringBuilder(len + 64).append(text, 0, j);

        // Scan the rest of the string, copying/replacing as we go
        do
        {
            final char c = text.charAt(j);
            switch (c)
            {
                case '\'':
                    str.append(SINGLE_QUOTE);
                    break;
                case '"':
                    str.append(DOUBLE_QUOTE);
                    break;
                case '&':
                    str.append(AMPERSAND);
                    break;
                case '<':
                    str.append(LESS_THAN);
                    break;
                case '>':
                    str.append(GREATER_THAN);
                    break;
                default:
                    str.append(c);
            }
        }
        while (++j < len);
        return str.toString();
    }

    /**
     * This will HTML encode the character buffer out to the writer in an efficient way.  If nothing needs to be encoded
     * its a pass through write. Otherwise it falls back to writing each character one at a time, expanding the Big5
     * html entities as it goes.
     * <p/>
     * This is intended to be used when your are writing a decorating html encoding writer.  The writes to delegate
     * writer will be done in as efficient manner as possible.
     *
     * @param writer the place to write HTML text to
     * @param cbuf the aray of characters to write
     * @param off the offset from which to start writing
     * @param len the number of characters to write
     * @throws IOException from the writer.write() calls
     */
    public static void encode(Writer writer, char[] cbuf, int off, int len) throws IOException
    {
        if (len == 0)
        {
            return;
        }
        for (int j = off; j < len; j++)
        {
            char c = cbuf[j];
            switch (c)
            {
                case '\'':
                case '"':
                case '&':
                case '<':
                case '>':
                    writeHeavy(writer, cbuf, off, len, j);
                    return;
            }
        }
        writer.write(cbuf, off, len);
    }


    private static void writeHeavy(final Writer writer, final char[] cbuf, final int off, final int len, final int from)
            throws IOException
    {

        int soFar = from - off;
        if (soFar > 0)
        {
            writer.write(cbuf, off, soFar);
        }

        for (int j = from; j < len; j++)
        {
            char c = cbuf[j];
            switch (c)
            {
                case '\'':
                    writer.write(SINGLE_QUOTE);
                    break;
                case '"':
                    writer.write(DOUBLE_QUOTE);
                    break;
                case '&':
                    writer.write(AMPERSAND);
                    break;
                case '<':
                    writer.write(LESS_THAN);
                    break;
                case '>':
                    writer.write(GREATER_THAN);
                    break;
                default:
                    writer.write(c);
            }
        }
    }

}